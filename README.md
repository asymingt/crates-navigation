# README #

Checkout code

```bash
git clone https://bitbucket.org/asymingt/crates-navigation.git
```

Install an ARM toolchain

```bash
sudo apt-get install gcc-arm-linux-gnueabi
```

Configure NuttX

```bash
cd nuttx/tools
./configure mikroe-mini-m4/ucl
cd ..
make oldconfig
```

Flash to Mini-M4

```bash
../misc/tools/mikroe-uhb/mikroe-uhb nuttx.hex
```