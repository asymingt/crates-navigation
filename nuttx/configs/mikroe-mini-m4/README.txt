GET THE KERNEL CONFIG

cd
git clone http://ymorin.is-a-geek.org/git/kconfig-frontends
cd kconfig-frontends/
sudo apt-get install gperf
sudo apt-get install flex
sudo apt-get install bison
sudo apt-get install libncurses5-dev
./bootstrap
./configure
make
sudo make install
sudo /sbin/ldconfig -v

CROSSDEV=arm-nuttx-eabi- make -j4
~/Workspace/Source/mikroe-uhb/mikroe-uhb nuttx.hex

UART MAPPINGS

U1 = BINR 115.2k@ 8O1
U2 = XBEE 115.2k@ 8N1
U3 = UBX OUT
U4 = ASCTEC HLP 57.6k @ 8N1

GPIO MAPPINGS

      SAFE  LAND  IDLE  DUCK 
PC4 =   1    0     1      0
PC5 =   1    1     0      0

MCLR---|       S1_MI|---PA6
 PA0---|U4_TX  S1_MO|---PA7
 PA1---|U4_RX  S1_CL|---PA5
 PA2---|U2_TX  S1_SS|---PA4
 PA3---|U2_RX       |---PA14
 PC0---|ADC1        |---PA13
PB12---|S2_SS       |---PC4
 PC1---|ADC2        |---PC5
 PC2---|ADC3        |---3.3V
 PA8---|            |---GND
3.3V---|            |---PA15
 GND---|            |---PB3
 PB9---|       I2SCL|---PB10
 PB8---|       I2SDA|---PB11
 PC9---|       U1_RX|---PB7
 PB4---|       U1_TX|---PB6
 PB5---|       S2_MO|---PB15
PB13---|S2_CL  S2_MI|---PB14
 PB0---|       U3_RX|---PC11
 PB1---|       U3_TX|---PC10

 Need to setup DMA