
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <debug.h>
#include <pthread.h>

// NuttX includes
#include <nuttx/config.h>
#include <nuttx/clock.h>
#include <nuttx/fs/fs.h>
#include <nuttx/usb/pl2303.h>

// Input and output UARTs
#define UART_SBC    "/dev/ttyUSB0"
#define UART_LLP    "/dev/ttyS3"
#define CHUNK       1
#define BUFFERLEN   128

// Ring buffer for LLP <-> SBC comms
#include "ringbuf.h"

// Pthread exit codes
enum exit_values_e
{
  TESTRESULT_SUCCESS = 0,
  TESTRESULT_PTHREAD_ATTR_INIT_FAIL,
  TESTRESULT_PTHREAD_CREATE_FAIL,
  TESTRESULT_PTHREAD_JOIN_FAIL,
  TESTRESULT_CHILD_ARG_FAIL,
  TESTRESULT_CHILD_RETVAL_FAIL,
};

static int f_sbc;
static int f_llp;

// Child thread SBC -> LLP (ring buffer)
void *sbc_to_llp(void *arg)
{
  ringbuf_t buf_sbc = ringbuf_new(BUFFERLEN);
  while(1)
  {
    ringbuf_read(f_sbc, buf_sbc, CHUNK);
    ringbuf_write(f_llp, buf_sbc, CHUNK);
  }
  pthread_exit(NULL);
}

// Child thread LLLP -> SBC (ring buffer)
void *llp_to_sbc(void *arg)
{
  ringbuf_t buf_llp = ringbuf_new(BUFFERLEN);
  while(1)
  {
    ringbuf_read(f_llp, buf_llp, CHUNK);
    ringbuf_write(f_sbc, buf_llp, CHUNK);
  }
  pthread_exit(NULL);
}


int ros_main(int argc, char *argv[])
{
  // Open devices
  usbdev_serialinitialize(0);
  sleep(1);
  f_sbc = open(UART_SBC, O_RDWR);
  f_llp = open(UART_LLP, O_RDWR);
  stm32_led_on(0);

  // Threaded listeners for TX and RX
  pthread_attr_t attr;
  pthread_t t1, t2;
  void *retval;
  int status;
  pthread_attr_init(&attr);
  pthread_create(&t1, &attr, sbc_to_llp, NULL);
  pthread_create(&t2, &attr, llp_to_sbc, NULL);
  pthread_join(t1, &retval);
  pthread_join(t2, &retval);

  // Clean up
  close(f_sbc);
  close(f_llp);
  return OK;

}